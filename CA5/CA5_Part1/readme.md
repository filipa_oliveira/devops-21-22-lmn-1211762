Class Assignment 5 - Part 1
===================

**IMPORTANT NOTE:** **This assignment was made in a forked repository, to which the teacher was already given access to. To see the commits and Jenkinfile please see the following link:**

```
https://bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762_v2/src/master/
```

The aim of this assignment is to practice with Jenkins using the "gradle basic demo" used in previous class assignments.

## What is Jenkins

Jenkins is a self-contained, open source automation server which can be used to automate all sorts of tasks related to building, testing, and delivering or deploying software. It supports version control tools such as Git and Mercurial.

### CI/CD Pipeline

CI stands for Continuous Integration and CD stands for Continuous Delivery and Continuous Deployment, and its purpose is to automate the process of the development lifecycle.

## Installing Jenkins

We can install Jenkins using various methods, such as through native system packages, Docker, or even run standalone by any machine with a Java Runtime Environment (JRE) installed.

I opted to install Jenkins with the war file that can be downloaded in [https://www.jenkins.io/download/].

I started to create a CA5 folder with a CA5_PART1 subfolder inside it. 
Next, I copied the war file inside this folder and opened a terminal at this folder and run the command:

```
java -jar jenkins.war
```

[1-jenkins-Upand-Runing.png](https://postimg.cc/LYwhdxts)

After this, I accessed the Jenkins application on `localhost:8080`. I chose the recommended pluggins and waited until it was finished:
Once the initial setup of Jenkins was done I proceeded to create a simple CI/CD pipeline.

## Creating jobs on Jenkins

### Fork the repository to be private

On previous class assignments I needed to change the access to our repositories and made it public. However, to test the jenkins credentials, I forked my repository and made the fork private.

[![2-Fork-Repository.png](https://i.postimg.cc/CKr521Kn/2-Fork-Repository.png)](https://postimg.cc/WDrsFsgs)

### Jenkins Credentials

When a private repository needs to be used, we need to give Jenkins the credentials to access and checkout the repository.

To make this possible, on the Jenkins server main page, on the left side, there is an option called `Manage Jenkins`.
After selecting go to `Security` and then `Manage Credentials`, I selected `global` and on the left side `Add Credentials` and this way I could add a new set of credentials.

- Username - Bitbucket username
- Password - Bitbucket password
- ID - Internal ID by which these credentials are identified from jobs and other configurations

[![3-credentials.png](https://i.postimg.cc/TwXgR0mv/3-credentials.png)](https://postimg.cc/rdQKjSQf)

After adding the necessary credentials, I created my first Job.

### Creating Jobs

To create a new job on Jenkins, it's necessary to access the server on `localhost:8080`, and create a new item.
After choosing a name for the job, I select the type.
In this case a **Pipeline.**

[![4-New-Pipeline.png](https://i.postimg.cc/nLKM1JsN/4-New-Pipeline.png)](https://postimg.cc/rdm887N9)

Once I reached the pipeline configuration menu, I selected that the pipeline definition would come from a `script from SCM`.

The type of SCM I am using is **Git** so, after selecting it, I inserted the repository URL and choose the Bitbucket credentials created previously.

Lastly, I defined the path of the Jenkinsfile, relative to the folder on the repository.

[![5-Configure-Pipeline.png](https://i.postimg.cc/nLn9grtn/5-Configure-Pipeline.png)](https://postimg.cc/w1bBs6vr)

## Jenkinsfile

The definition of a Jenkins Pipeline is typically written into a text file (called a Jenkinsfile) and it supports two syntaxes, Declarative and Scripted Pipeline. Both of which support building continuous delivery pipelines.

In this Jenkinsfile there are **5 stages**:

- Agent - The agent directive, which is required, instructs Jenkins to allocate an executor and workspace for the Pipeline
- Checkout - This stage will checkout the source code for the project on the defined URL using the credentials with the defined ID (this is the same internal ID defined in Jenkins).
- Assemble - This stage is going to run the Gradle `assemble` task of the project so that it will build its artifacts.
- Test - This stage runs the `test` task of the project, that runs all the unit tests.
- Archiving - This stage archives the build artifacts (for example, distribution zip files or jar files) so that they can be downloaded later. Archived files will be accessible from the Jenkins webpage.

```groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'fo-bitbucket-credentials', url: 'https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762_v2/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA5/CA5_Part1/gradle_basic_demo; ./gradlew assemble'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA5/CA5_Part1/gradle_basic_demo; ./gradlew test'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'CA5/CA5_Part1/gradle_basic_demo/build/distributions/*'
            }
        }
    }
    post {
        always {
            junit '**/build/test-results/test/*.xml'
        }
    }
}
```

## Running Jobs

Now that the Jenkinsfile is created and on the root folder of the project, I can now tell Jenkins to run the new CI/CD pipeline. 

NOTE: My firsts builds failed because the path was wrong, then the stage assemble failed too because the path to the Jenkinsfile was wrong too. After some attempts and once everything was solved I finally got it:

[![7-jenkins-Build-OK.png](https://i.postimg.cc/2SSftNXP/7-jenkins-Build-OK.png)](https://postimg.cc/56TGHTJ3)

Image with the post action of publishing test results:

[![8-With-Test-Results.png](https://i.postimg.cc/MHmhs906/8-With-Test-Results.png)](https://postimg.cc/1V4CXcY2)

## References
https://jenkins.io/doc/book/pipeline/jenkinsfile/

https://www.edureka.co/blog/ci-cd-pipeline/