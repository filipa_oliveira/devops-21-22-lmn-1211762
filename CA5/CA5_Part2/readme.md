Class Assignment 5 - Part 2
===================

The aim of this assignment is to create a pipeline in Jenkins to build one of the applications used in previous class assignments.
I opted to use the "gradle basic demo", since I had already been working with that app in Part1 1.

### Bitbucket Issues

In this CA I created some issues on Bitbucket to track the development of the application.

Most important issues created during the development of this assigment:

issue #46 - Create Jenkinsfile 
issue #47 - Add Javadoc
issue #48 - Create Dockerfile
issue #49 - Add Docker Image stage 

## Create Pipeline

Similar to the part 1 of this assignment, we need to build a CI/CD pipeline to automate the process of the development lifecycle.

I started by creating a new job on my Jenkins server. But first, I made sure I went to the folder where I had the _.war_ file and run the command :

```
java -jar jenkins.war
```

Next, I opened my localhost and logged in to my Jenkins.

I created a new Item the same way I did for the first part of this CA.

- New Item
- Insert the item name
- Choose pipeline
- Pipeline script from SCM - Git - Repository URL - Same credentials defined on part 1

[![1-configure-pipeline.png](https://i.postimg.cc/SRW4L9fW/1-configure-pipeline.png)](https://postimg.cc/m12KBtqD)

The _Jenkinsfile_ produced in this second part is built similarly to the first one.
The first part will be the same:

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		git credentialsId: 'fo-bitbucket-credentials', url: 'https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew assemble'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew test'
                junit '**/build/test-results/test/*.xml'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo/build/libs; ls -al'
                archiveArtifacts 'CA5/CA5_Part2/gradle_basic_demo/build/libs/*'
            }
        }
    }
}
```

Next, as mentioned in the teacher's slides, it is necessary to add some stages:

- Javadoc: to generate the javadoc of the project and publish it in Jenkins.
- Publish Image: generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

In order to do this I needed to install some plugins to my Jenkins. So, in my Jenkins dashboard I selected Manage Jenkins - Manage plugins and then chose:

- Javadoc
- Docker Pipeline
- HTML Publisher

[![2-Install-Plugins.png](https://i.postimg.cc/3xhRq8y8/2-Install-Plugins.png)](https://postimg.cc/F7PNdQg8)

Once I was all set, I started to prepare the files.

## Publish the HTML

In this assignment we were asked to publish the HTML files in Jenkins.

To help add the publishing step to this task, I used Jenkins **Snippet Generator** to generate the code.

On the Jenkins main page, I select the job created above and then selected **Pipeline Syntax**

Next, I chose the **publishHTML: Publish HTML reports** step, and specified the path where the generated HTML files were stored.

[![3-Snippet-Generator.png](https://i.postimg.cc/fLGxVVC4/3-Snippet-Generator.png)](https://postimg.cc/ygTJ5Y2n)

After pressing the **Generate Pipeline Script** button, a line of code is generated, that can then be inserted in the Jenkinsfile stage. It's important to add the `target:` that isn't included in the script.

[![4-HTML-Script.png](https://i.postimg.cc/VvMnczjY/4-HTML-Script.png)](https://postimg.cc/GTLHFw7N)

In order to generate javadoc with Jenkinsfile I added this stage:

```
stage('JavaDoc') {
            steps {
                echo 'Generating JavaDoc...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: false,
                        reportDir: 'CA5/CA5_Part2/gradle_basic_demo',
                        reportFiles: 'index.html',
                        reportName: 'HTML Report',
                        reportTitles: ''
                        ])
            }
        }
```

To help understand this stage:

- allowMissing : if it's true will allow the build to run if the report is missing.
- allwaysLinkToLastBuild and keepAll : if checked, it will keep all past HTML reports, even if build failed.
- reportDir : the path to the HTML report directory relative to the workspace.
- reportFiles : the file to provide links inside the report directory.
- reportName : the name of the report to display for the build/project

## Dockerfile

To make Jenkins generate a docker image I used the  Dockerfiles that we learned and practised in the previous class assignment. Next, I placed it in the same folder as the Jenkinsfile.

For this class assignment I only needed to initialize the app with Tomcat, so I made a simpler Dockerfile:

```
FROM tomcat

RUN apt-get update -y

COPY ./build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ 

EXPOSE 8080
```

Afterwards, it was asked to publish the docker image at Docker Hub, so I created the Docker Hub credentials in my Jenkins, like I did for Bitbucket:

[![4-Jenkin-Docker-Hub-credentials.png](https://i.postimg.cc/kXvnN6xv/4-Jenkin-Docker-Hub-credentials.png)](https://postimg.cc/HVjDdLvr)

## Jenkinsfile

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		git credentialsId: 'fo-bitbucket-credentials', url: 'https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew assemble'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew test'	
	            junit '**/build/test-results/test/*.xml'
            }
        }

	stage('JavaDoc') {
            steps {
                echo 'Generating JavaDoc...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo; ./gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: false,
                        reportDir: 'CA5/CA5_Part2/gradle_basic_demo',
                        reportFiles: 'index.html',
                        reportName: 'HTML Report',
                        reportTitles: ''
                        ])
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo/build/libs; ls -al'
                archiveArtifacts 'CA5/CA5_Part2/gradle_basic_demo/build/libs/*'
            }
        }
   

	stage ('Docker Image'){
            steps{
                echo 'Building and pushing Image...'
                sh 'cd CA5/CA5_Part2/gradle_basic_demo'
                script{
                    def customImage = docker.build("filipaoliveira1211762/devops-21-22-lmn-1211762:${env.BUILD_NUMBER}", './CA5/CA5_Part2/gradle_basic_demo')
                    docker.withRegistry( 'https://registry.hub.docker.com', 'fo-dockerhub-credentials') {
                        customImage.push("${env.BUILD_NUMBER}")
                }
            }
        }

     }
}}
```


## Build Errors

Unfortunately, I encountered several errors in the process of this CA. Luckily, by using the Jenkins output console and each stage Logs I was able to see where the build was breaking and most of the times why. 
This pointed me in the direction of the solution for the errors and helped me in the process of making the necessary corrections.
As an example, one of the errors I encountered was during the Assemble stage, where jenkins could not find the graddle wrapper:

[![5-Error-on-Gradle-Wrapper.png](https://i.postimg.cc/8ctwmmw6/5-Error-on-Gradle-Wrapper.png)](https://postimg.cc/PvvmtZgX)

I was able to correct this error by force pushing the Graddle Wrapper as we can see in the image below:

[![6-Solve-Error-on-Gradle-Wrapper.png](https://i.postimg.cc/6qfpQYwF/6-Solve-Error-on-Gradle-Wrapper.png)](https://postimg.cc/BPt0Y5jC)

Other errors were encountered, some related with path and directories, others with credentials, or even related to .gitignore... Please see remote repository commits for details and image below: 

[![7-Build-with-errors.png](https://i.postimg.cc/L4TP5XnG/7-Build-with-errors.png)](https://postimg.cc/MXnHrz0Y)

## Successful pipeline build

After several attempts and commits, I was able to make a successful Build as we can see in the image below:

[![8-Build-SUCCESS-with-test-result.png](https://i.postimg.cc/pVF063Xs/8-Build-SUCCESS-with-test-result.png)](https://postimg.cc/HjT4JZMM)

After, the successful build the Docker image was created and published in DockerHub as we can see in the image bellow:

[![9-Image-published-success-in-Docker-hub.png](https://i.postimg.cc/sX64B9cH/9-Image-published-success-in-Docker-hub.png)](https://postimg.cc/JDXkvXSj)

Lastly, I opened my Docker Desktop app and run the docker image with tag 19 (number of my jenkins successful build):

[![10-docker-desktop-running.png](https://i.postimg.cc/QNvn0hQ4/10-docker-desktop-running.png)](https://postimg.cc/PNz4qGFY)

__________________________

## 2. Alternative Analysis - Buddy

My chosen alternative was Buddy (https://buddy.works/). I choose this alternative because of the User Interface, which is really simple and does not require to have a configuration file.

There are several advantages using Buddy:

- Effortless pipeline configuration in a telling GUI with a wide range of predefined actions;
- Simple installation;
- Dedicated Integrations: easily integrate with AWS, Google, Docker and Kubernetes accounts;
- Easy to learn;

Now, let's look to a quick comparison to Jenkins:

[![1-buddy-versus-jenkins.png](https://i.postimg.cc/Wbr4WGyB/1-buddy-versus-jenkins.png)](https://postimg.cc/kBJ9Dbqs)

After the implementation I realized that buddy is way more simple to configure and is faster to build pipelines!

## 3. Implementation of the Alternative

First, I signed-in the website and synchronized with my Bitbucket account; fetching the project repository directly to my Buddy account.

[![Screenshot-2022-06-06-at-22-51-09.png](https://i.postimg.cc/XYf0FxPv/Screenshot-2022-06-06-at-22-51-09.png)](https://postimg.cc/HrkK10dG)
[![Screenshot-2022-06-06-at-22-53-13.png](https://i.postimg.cc/85JGFDQ2/Screenshot-2022-06-06-at-22-53-13.png)](https://postimg.cc/cgS2jpJh)

### Pipeline

Next, I chose pipeline and add a new pipeline:

[![Screenshot-2022-06-06-at-22-55-08.png](https://i.postimg.cc/pX1BCtgG/Screenshot-2022-06-06-at-22-55-08.png)](https://postimg.cc/KRBLcCcB)

### Actions

Next, I added the required actions to my pipeline:

First, the assemble action:

I chose gradle action and then in the **Run** separator I used the command to assemble the application:

[![Screenshot-2022-06-06-at-23-01-55.png](https://i.postimg.cc/x8pL6KFJ/Screenshot-2022-06-06-at-23-01-55.png)](https://postimg.cc/Zv39Rym4)

Let's do the same for the test action:

[![Screenshot-2022-06-06-at-23-04-12.png](https://i.postimg.cc/gjjZLxkY/Screenshot-2022-06-06-at-23-04-12.png)](https://postimg.cc/5Qc0ZNCG)

And javadoc:

[![Screenshot-2022-06-06-at-23-07-08.png](https://i.postimg.cc/Y9n47tNT/Screenshot-2022-06-06-at-23-07-08.png)](https://postimg.cc/jwfqQ0Fh)

(I didn't found a way to publish the HTML but all the results and reports from tests and javadoc can be found in the pipeline filesystem and you can open then in browser and see the reports)

Now, for the archive action we need to create a folder inside the demo folder so we can save the zip file there. Let's choose the action with the zip file and configure:

[![Screenshot-2022-06-06-at-23-10-34.png](https://i.postimg.cc/QCvHBSyb/Screenshot-2022-06-06-at-23-10-34.png)](https://postimg.cc/sG9VqPyQ)

And finally the action Build Docker Image and select the path to de Dockerfile:

[![Screenshot-2022-06-06-at-23-13-08.png](https://i.postimg.cc/cJTC7dz6/Screenshot-2022-06-06-at-23-13-08.png)](https://postimg.cc/GB8bdnd1)

and then we want to publish it at Docker Hub:

[![Screenshot-2022-06-06-at-23-17-06.png](https://i.postimg.cc/C58fsWDt/Screenshot-2022-06-06-at-23-17-06.png)](https://postimg.cc/XBVJV1sc)


Now let's try and run our pipeline!

[![Screenshot-2022-06-06-at-23-18-52.png](https://i.postimg.cc/9FG7RHcw/Screenshot-2022-06-06-at-23-18-52.png)](https://postimg.cc/3dxRV643)


