Class Assignment 4 - Part 2
===================
## 1. Analysis, Design and Implementation

The aim of this assignment is to use Docker to set up a containerized environment to execute our version of the gradle version of the spring basic tutorial application

### Containerization with Docker

The objective is to deploy the application to two containers. The first container will run the web part of the application, to run Tomcat and spring application and the second will be used to execute the H2 server database.

### Containers

What is a container and why do we need them?
In a brief summary a container is a standard unit of software that packages up code and all its dependencies, so the application runs quickly and reliably from one computing environment to another.
Containers are an abstraction at the app layer that packages code and dependencies together, sharing the host operating system, being much more lightweight than VM's.
We need containers because by containerizing the application platform and its dependencies, differences in OS distributions and underlying infrastructure are abstracted away.

### Docker compose

Compose is a tool for running multi-container Docker applications.
With Compose, we use a `YML` file to configure our containers, and with a single command control all the services from the configuration.

### Dockerfile

Docker can build images automatically with a Dockerfile. It's a text file that contains all the commands a user can call on the command line to assemble the Docker image.
In order to have more than one Dockerfile, I had to create folders with the name of the container I wanted to create, in our example, I had to create the db folder and the web folder, and within each exists the respective Dockerfile.

### Adding folders, Dockerfile and YML file

I added 2 new folders inside CA4/Part2 and a YML file:
- web
- db
- docker-compose.yml

I used the Dockerfile and the yml file examples that were available in the teacher's repository but made the necessary changes:
- Change the IP
- Change path to my own repository
- Change the work directory (WORKDIR)
- The ports I was using previously were the same, so there was no need to change these.


> Inside the _web_ folder I created a Dockerfile where I gave instructions to start my container with tomcat and cloned my repository inside it:

...

FROM tomcat:8-jdk8-temurin

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762.git

WORKDIR /tmp/build/devops-21-22-lmn-1211762/CA3/CA3Part2/react-and-spring-data-rest-basic

RUN ./gradlew clean build

RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

RUN rm -Rf /tmp/build/

EXPOSE 8080
...

> Inside de _db_ folder I also added a Dockerfile to create another container for my app database:

...

FROM ubuntu

RUN apt-get update && \
apt-get install -y openjdk-8-jdk-headless && \
apt-get install unzip -y && \
apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
...


> And the yml file contents:

...

version: '3'
services:
web:
build: web
ports:
- "8080:8080"
networks:
default:
ipv4_address: 192.168.56.10
depends_on:
- "db"
db:
build: db
ports:
- "8082:8082"
- "9092:9092"
volumes:
- ./data:/usr/src/data-backup
networks:
default:
ipv4_address: 192.168.56.11
networks:
default:
ipam:
driver: default
config:
- subnet: 192.168.56.0/24

...

After all the necessary alterations were made in both Dockerfiles and the YML file I committed and pushed the changes:
- **git add.**
- **git status**
- **git commit -a -m "(Resolves #34) Create folder Part2 inside CA4, create subfolders db and web and respective Dockerfiles, create docker-compose.ym"**
- **git push**

[![Screenshot-2022-05-18-at-16-05-36.png](https://i.postimg.cc/jSNxK4K7/Screenshot-2022-05-18-at-16-05-36.png)](https://postimg.cc/hXDW1xXD)

### Compose up

Now that everything was ready, I tested the 'compose up' command to test if I could create the 2 containers: 
- **docker-compose up**

[![Screenshot-2022-05-18-at-16-09-00.png](https://i.postimg.cc/JzBNLvG5/Screenshot-2022-05-18-at-16-09-00.png)](https://postimg.cc/pmRhQ0Qm)

Unfortunately as we can see in the image above, an error occurred because of a typo error in the dockerfile: 'hhttps://' instead of 'http://'.
After correcting this, I ran the command docker-compose up again:

[![Screenshot-2022-05-18-at-16-32-33.png](https://i.postimg.cc/XYb30QKy/Screenshot-2022-05-18-at-16-32-33.png)](https://postimg.cc/tY283dMX)

This time, a different error was found. As an attemptive solution I decided to separate in 1 line command into 3:

[![Screenshot-2022-05-18-at-16-33-50.png](https://i.postimg.cc/44VSTZWB/Screenshot-2022-05-18-at-16-33-50.png)](https://postimg.cc/4n456Ck9)

I ran docker-compose up once more and noticed the error persisted and this time appeared in the section 'RUN ./gradlew clean build'.
I noticed it as another typo error in the web Dockerfile, there was a missing slash in the WORKDIR path, that I correct to:
*WORKDIR /tmp/build/devops-21-22-lmn-1211762/CA3/CA3Part2/react-and-spring-data-rest-basic*

After saving the changes I ran the docker-compose up once again and a new error was found, this time concerting the IP in the docker-compose.yml file:

[![Screenshot-2022-05-18-at-16-52-48.png](https://i.postimg.cc/DZZnM0rj/Screenshot-2022-05-18-at-16-52-48.png)](https://postimg.cc/TyZzyRHg)

After verifying that the IP addresses in the yml file were correct, I used the command *docker network prune* command to remove all host networks that weren't used by at least one container and finally the application build successfully as we can see in the next images:

[![Screenshot-2022-05-18-at-16-55-37.png](https://i.postimg.cc/PxTsgxms/Screenshot-2022-05-18-at-16-55-37.png)](https://postimg.cc/948nTc8J)
[![Screenshot-2022-05-18-at-16-56-04.png](https://i.postimg.cc/TYpMMBWw/Screenshot-2022-05-18-at-16-56-04.png)](https://postimg.cc/XpSD9HLS)

Container running:

[![Screenshot-2022-05-18-at-17-12-26.png](https://i.postimg.cc/W4s5jJfn/Screenshot-2022-05-18-at-17-12-26.png)](https://postimg.cc/BjwTBt3L)

Now that I had the frontend and database running on docker! I checked my localhost the same way I did in the last assignment:

[![Screenshot-2022-05-18-at-16-58-01.png](https://i.postimg.cc/gjh7rbyq/Screenshot-2022-05-18-at-16-58-01.png)](https://postimg.cc/QKXmnPyH)

[![Screenshot-2022-05-18-at-17-00-09.png](https://i.postimg.cc/MHYy9szJ/Screenshot-2022-05-18-at-17-00-09.png)](https://postimg.cc/K3RkzDK0)

[![It-works.png](https://i.postimg.cc/fRPYqSQy/It-works.png)](https://postimg.cc/QFpBHttZ)

[![Screenshot-2022-05-18-at-17-03-19.png](https://i.postimg.cc/6ptRB4NM/Screenshot-2022-05-18-at-17-03-19.png)](https://postimg.cc/D8pmgZ0G)

Next, it was necessary to publish the images to Docker hub. I used my Docker hub account and the repository created in CA4 Part1 assignment. 
To do this, I started by running the command *docker image ls* so I could see all the images created, followed by the command *docker tag* to create a tag for each one of the images:

[![Screenshot-2022-05-18-at-17-24-18.png](https://i.postimg.cc/ZRNkvN4d/Screenshot-2022-05-18-at-17-24-18.png)](https://postimg.cc/N9QCW5ys)

I pushed the image part2_db to my repository using **docker push**:

[![Screenshot-2022-05-18-at-17-33-34.png](https://i.postimg.cc/PxVLJBfx/Screenshot-2022-05-18-at-17-33-34.png)](https://postimg.cc/jwNqk370)

I also pushed the image part2_web to my repository using **docker push**:

[![Screenshot-2022-05-18-at-18-12-31.png](https://i.postimg.cc/J7PwDRWK/Screenshot-2022-05-18-at-18-12-31.png)](https://postimg.cc/HjcvFDn7)

Both images were pushed correctly:

[![Screenshot-2022-05-18-at-18-04-05.png](https://i.postimg.cc/VNDTSFxv/Screenshot-2022-05-18-at-18-04-05.png)](https://postimg.cc/PLvK91rn)

### Volumes

Volumes are the preferred mechanism for persisting the data generated and used by Docker containers, the volumes are fully managed by Docker. A volume does not increase the size of the containers that uses it, and the content of the volume exists outside the life cycle of a given container.

Lastly it was necessary to use volumes to persist the data by using the command:
- **docker compose exec db bash**

[![Last-login-Wed-May-18-172705-on-ttys001.png](https://i.postimg.cc/L55M68Nq/Last-login-Wed-May-18-172705-on-ttys001.png)](https://postimg.cc/sQkLTsFy)

The volume was set in docker-compose.yml:

```
  volumes:
      - ./data:/usr/src/data
```

[![Screenshot-2022-05-18-at-18-03-15.png](https://i.postimg.cc/MTZCdGks/Screenshot-2022-05-18-at-18-03-15.png)](https://postimg.cc/67SbQtpR)
________________________________________


## 2.Alternative Analysis - Kubernetes

The chosen alternative was (Kubernetes)[https://www.heroku.com].

Kubernetes is an open-source container orchestration platform for managing, automating, and scaling containerized applications. 

Organizations use Kubernetes to automate the deployment and management of containerized applications. Rather than individually managing each container in a cluster, a DevOps team can instead tell Kubernetes how to allocate the necessary resources in advance.

### Docker Container Problems:
- How can all of these containers be coordinated and scheduled?
- How do we seamlessly upgrade an application without any interruption of service?

Solutions for orchestrating containers soon emerged: Kubernetes, Mesos, and Docker Swarm are some of the more popular options.

### Differences between Kubernetes & Docker 

A major difference between Docker and Kubernetes is that Docker runs on a single node, whereas Kubernetes is designed to run across a cluster.  Another difference between Kubernetes and Docker is that Docker can be used without Kubernetes, whereas Kubernetes needs a container runtime in order to orchestrate.
Where Kubernetes and the Docker suite intersect is at container orchestration. So when we talk about Kubernetes vs. Docker, what we really mean is Kubernetes vs. Docker Swarm.

### Kubernetes vs. Docker Swarm

Both Docker Swarm and Kubernetes are production-grade container orchestration platforms, although they have different strengths.
Docker Swarm, also referred to as Docker in swarm mode, is the easiest orchestrator to deploy and manage. It can be a good choice for an organization just getting started with using containers in production. Swarm solidly covers 80% of all use cases with 20% of Kubernetes’ complexity.

| Kubernetes      | Docker Swarm |
| ----------- | ----------- |
| Complex installation	     | Easier installation|
| More complex with a steep learning curve, but more powerful   | Lightweight and easier to learn but limited functionality        |
| Supports auto-scaling	 | Manual scaling|
| Built-in monitoring	 | Needs third party tools for monitoring |
| Manual setup of load balancer	 | Auto load balancer |
| Need for separate CLI tool	 | Integrated with Docker CLI|


### References

- https://kubernetes.io/
- https://www.dynatrace.com/news/blog/kubernetes-vs-docker
- https://www.dynatrace.com/news/blog/kubernetes-vs-docker/

