Class Assignment 4 - Part 1
===================
### Containers & Dockers 

**Docker** is, essentially, software that uses virtualization to deliver software packages in the form of containers.

**Containers** are lightweight, isolated environments that share a single operating system and bundle their own software, libraries and configuration files.

**Images** are container templates that can easily store and ship applications.

### Getting started with Docker 

After installing docker desktop for mac in my computer I decided to verify the version that was installed using the terminal to run the command: <br>
- **docker --version** <br>

And the version installed was **Docker version 20.10.14** <br>

Next, I installed the container **docker run --name my-ubuntu-container -i -t ubuntu /bin/bash** <br>
The operating system ubuntu is the image that corresponds to the box. <br>
The -i and -t indicates the creation of an interactive session. <br>

The container **my-ubuntu-container** in which I want to run the image ubuntu, is in its term running Linux shell /bin/bash. <br>
After running in the terminal, I obtained the following reference: **root@root@47e8484116a4**, indicating that I am inside the ubuntu and the bash.

Next, I opened my CA4/Part1 terminal and by using the command: <br>
- **docker ps** <br>

it was possible to see the containers running, showing an id -**47e8484116a4**- , that corresponds to the ubuntu image that is running the bash command. <br>
Now, if I want, I can stop and delete the container just by using a command + id: <br>

To stop the container: 
- **docker stop 47e8484116a4** <br>

To eliminate the container: 
- **docker rm 47e8484116a4** <br>

By using the **docker ps** command , I could verify that the container was deleted. If I want to run the container again, it is possible by using Docker installed in my machine to do it quickly. <br>
However, everytime we delete and create a container, the id changes.

Finally, in my Part1 folder terminal and by using the **docker images** command, I notice another id exists -**d2e4e1f51132**. In this instance, the image id is respective to the box.

There are other states that can be attributed to containers + ID:

- docker pause
- docker unpause
- docker start
- docker restart
- docker stop
- docker run
- docker kill

___________________

## CA4 Part1 - Version A

In this version I built the chat server "inside" the Dockerfile

I started by adding a 'VersionA' subfolder inside Part1, and creating the respective Dockerfile inside it:

> FROM ubuntu:xenial <br>
> RUN apt-get update <br>
> RUN apt-get install -y openjdk-8-jdk-headless <br>
> RUN apt-get install git -y <br>
> RUN git clone https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762.git <br>
> WORKDIR /gradle_basic_demo <br>
> RUN chmod u+x gradlew <br>
> RUN ./gradlew clean build <br>
> EXPOSE 59001 <br>
> CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 <br>

Next I created the image in the terminal by using the command: <br>
- **docker build -t ubuntu .**

To confirm the image was actually created, I used the command: <br>
- **docker images**

In my terminal Part1/VersionA I ran the image ubuntu, and to confirm the existence of the images and containers I used the commands: <br>
- **docker run -p 59001:59001 -d ubuntu** <br>
- **docker ps -a**

Finally, I was able to simulate the chat by using the following commands in Part1/VersionA terminal: <br>
- **./gradlew build** <br>
- **./gradlew runClient**

Lastly, I logged into Docker Hub and used the docker push command to save my container images in my Docker hub repository: <br>
- **docker push filipaoliveira1211762/devops-21-22-lmn-1211762:ubuntu**

______________

## CA4 Part1 - Version B

In this version I built the chat server in my host computer and copied the jar file "into" the Dockerfile.

I started by adding a 'VersionB' subfolder inside my CA4/Part1 folder and creating the respective Dockerfile:

> FROM ubuntu:18.04 <br>
> RUN apt update <br>
> RUN apt install openjdk-8-jdk openjdk-8-jre-headless -y <br>
> COPY basic_demo-0.1.0.jar . <br>
> EXPOSE 59001 <br>
> CMD ./gradlew runServer <br>

Afterwards I copied the file 'basic_demo-0.1.0.jar' from devops-21-22-lmn-1211762/CA2/Part1/gradle_basic_demo/build/libs and pasted it into devops-21-22-lmn-1211762/CA4/Part1/VersionB

Lastly, in versionB terminal folder, I used the following command to successfully run the application:
- **docker build .**  

FINAL NOTE: This task (version B) was much simpler and less time-consuming than version A.











