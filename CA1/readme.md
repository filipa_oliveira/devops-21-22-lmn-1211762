# Class Assignment nº1

### Analysing & Implementing Git

### CA1 Part 1
As instructed, in this Part 1 of the exercise everything was developed without using branches (everything in the master branch).
<br>

I started by using my own repository devops-21-22-lmn-1211762 created the week before and created a new folder named "CA1" specifically for this assignment and copied into it the application inside the "basic" folder of the Tutorial React.js and Spring Data REST.
Two issues were created in bitbucket with the tasks:
- ISSUE #1 : Create CA1 folder.
- ISSUE #2 : Create Readme.md file inside CA1 folder.

To accomplish these tasks I used the following git commands:
- Creation of the directory inside devops-21-22-lmn-1211762:
     >    - **mkdir CA1**
- Move inside the directory
     >    - **cd CA1/**
- Creation of readme.md
     >    - **touch readme.md**
     
<br>

Afterwards I made some changes in the application and did several commits.
   - Changes to be committed to the remote repository were added to the staging area:
        >    - **git add .**
   - It was possible to commit the changes with a message or description by using the command:
          >      - **git commit -m "example message for this commit"**
   - To push the changes added to the staging area into my remote repository I used:
        >    - **git push**
   - As required I was able to create the tag v1.1.0 to mark the initial version:
        >    - **git tag -a v1.1.0 -m "initial version V1.1.0"**
   - I then pushed the tag v1.1.0 to the remote repository to be associated to the last commit:
        >    - **git push origin v1.1.0**
   - Finally, and at the end of completion of the exercise, when the new feature "JobYears" was created and fully tested I pushed it to the remote repository with a new tag v1.2.0:
        >    - **git push origin v1.2.0**
   - **IMPORTANT NOTE:** at this point I did not know the same commit could have two different tags, so I decided to do one more commit (with a minor alteration) to represent the end of the exercise, call it "test commit" and tag it with ca1-part1 to mark the end of the assignment in my repository, using the command:
      >    - **git push origin ca1-part1**

<br>

****

### CA1 Part 2
In this second part of the assignment we were instructed to work in different branches to practise the creation and merge of different branches.
- In order to implement a new feature I created a new branch named "email-field":
  >    - **git branch email-field**
- The above command creates the new branch but does not switch to it, in order to switch from the main branch to the new one I used:
  >    - **git checkout email-field**
- Once the code was updated with the new methods in Class Employee and support was added, I made a commit and pushed it to the remote repository:
> - **git add.**
> - **git commit -m "email-field created and support added"**
> - **git push email-field**
- When all the unit tests were made and the new feature was finalized I committed the changes and tagged it with v1.3.0 and merged to the master branch:
> - **git tag v1.3.0**
> - **git push origin v1.3.0**
> - **git checkout master**
> - **git merge email-field**

- It was then suggested we create a new branch to fix bugs. I created a branch named fix-invalid-email:
> - **git checkout -b fix-invalid-email**
- After creation of the method validateEmail() that ensures emails registered in the application contain a prefix followed by a @, the alterations were committed and pushed into the new branch:
> - **git add .**
> - **git commit -m "creation of method validateEmail()"**
- To push the changes to the remote repository:
 >  - **git push origin fix-invalid-email**
- Following this push I merged the branch fix-invalid-email with master branch and tagged this version as v1.3.1:
 >   - **git merge fix-invalid-email**
 >   - **git push origin**
 >  - **git tag v1.3.1**
 >  - **git push origin v1.3.1**
- Once this part 2 of the assignment was completed I marked the repository with tag ca1-part2:>
> - **git tag ca1-part2**
> - **git push origin ca1-part2**
<br>

****

