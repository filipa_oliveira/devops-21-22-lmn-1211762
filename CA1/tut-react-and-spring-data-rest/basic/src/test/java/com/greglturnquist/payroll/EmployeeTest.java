package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void testCreationOfIdenticalEmployees() {
        Employee employee1 = new Employee("John", "Doe", "dummy description", "manager", 3, "john@email.com");
        Employee employee2 = new Employee("John", "Doe", "dummy description", "manager", 3, "john@email.com");
        assertEquals(employee1, employee2);
    }

    @Test
    void testCreationOfDifferentEmployees() {
        Employee employee1 = new Employee("John", "Doe", "dummy description", "manager", 3, "john@email.com");
        Employee employee2 = new Employee("Jane", "Doe", "dummy description", "associate", 1, "jane@email.com");
        assertNotEquals(employee1, employee2);
    }

    @Test
    void testInvalidFirstNameBecauseNull() {
        String expectedErrorMessage = "First name is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee(null, "Doe",
                    "dummy description", "manager", 3, "john@email.com");
        });
        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    void testInvalidBecauseFirstNameBecauseEmpty() {
        String expected = "First name is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Doe", "dummy description", "manager",  3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidLastNameBecauseNull() {
        String expected = "Last name is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", null, "dummy description", "manager",  3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidLastNameBecauseEmpty() {
        String expected = "Last name is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "", "dummy description", "manager",  3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidDescriptionBecauseNull() {
        String expected = "Description is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", null, "manager",  3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidDescriptionBecauseEmpty() {
        String expected = "Description is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "", "manager",  3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidJobTitleBecauseNull() {
        String expected = "Job title is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", null, 3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidJobTitleBecauseEmpty() {
        String expected = "Job title is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "", 3, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testJobYearsBelowZeroExpectedInvalidMessage() {
        String expected = "Job years is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", -3, "john@email.com" );
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testJobYearsAbove100ExpectedInvalidMessage() {
        String expected = "Job years is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager",  105, "john@email.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidEmailBecauseNull() {
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3, null);
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void testInvalidEmailBecauseEmpty() {
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3, "");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmailAddressForbiddenCharacterPrefix(){
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3,  "forbid#den@domain.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmailAddressDotAtStartPrefix(){
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3,  ".forbidden@domain.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmailAddressMissingPrefix(){
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3,  "@domain.com");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmailAddressMissingDomain(){
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3,  "forbidden@");
        });
        assertEquals(expected, exception.getMessage());
    }

    @Test
    void createEmailNotAnEmailAddress(){
        String expected = "Email is invalid";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("John", "Doe", "dummy description", "manager", 3,  "namemail.com");
        });
        assertEquals(expected, exception.getMessage());
    }

}