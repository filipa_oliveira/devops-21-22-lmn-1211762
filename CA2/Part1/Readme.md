Class Assignment 2 - Part 1
===================

The aim of this assignment is to practise gradle.
Firstly, I started by creating a CA2 folder and a Part1 sub-folder using the git mkdir command. Secondly I downloaded and committed to my repository devops-21-22-lmn-1211762, the example application that was given.
- **git mkdir Part1**
- **git add .**
- **git commit -m "create Part1 folder inside CA2 folder"**
- **git push**
> Only afterwards I realized I forgot to include teh Issue number in the previous commit message, so I used a command to replace the message of my last commit with a new one:
- **git commit --amend -m "(resolves #3)create Part1 folder inside CA2 folder**
- **git push --progress origin --force**

This assignment also instructed us to create a Readme.md file inside the folder Part1:
- **git touch Readme.md**
- **git add .**
- **git status**
- **git commit -m "(Resolves #4) create Readme.md file inside Part1 folder"**
- **git push**

In terms of tasks the Part 1 of this assignment included 4 tasks.

Task 1 - Add a new task to execute the server
===================

In the gradle_basic_demo terminal folder I ran the command: 
- **./gradlew build** 

The output in the terminal was that the Build was successful. 

Next I opened the application on my IDE to update the build.gradle file and created a task runServer with the following code: 

>*task runServer(type:JavaExec, dependsOn: classes){* \
*group = "DevOps"* \
*description = "Launches a chat server that connects to a server on localhost:59001"* \
*classpath = sourceSets.main.runtimeClasspath* \
*mainClass = 'basic_demo.ChatServertApp'* \
*args '59001'* \
*}* 

Next, I opened a gradle_basic_demo terminal folder and, as instructed, ran the command: 
- **java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>** 

I then opened a second gradle_basic_demo terminal and ran the command: 
- **./gradlew runClient** 

After running these commands a new chat window automatically popped up as expected. \
I then committed and pushed the alterations made to the application by using: 
- **git commit -m "(Resolves #5) execute server"**
- **git push**


Task 2 - Add a simple unit test and update the gradle script.
===================

For the completion of this task, I started by opening the build.gradle file and add under dependencies: 
- **testImplementation('junit:junit:4.12')** 

I then created the path *src/test/java/basic_demo/AppTest.java* and pasted the unit test provided inside AppTest.java

Afterwards, in the gradle_basic_demo terminal I ran the command:
- **./gradlew test**

The terminal output was that the build was successful.\
Finally, I committed and pushed the alterations to my remote repository by using:
- **git add .**
- **git commit -m “(Resolves #6) add unit test and execute test”**
- **git push**


Task 3 - Add a new task of type Copy to be used to make a backup.
===================

To complete this task I opened the application on my IDE to update the build.gradle file and create a task Copy with the following code:
>*task copy(type:Copy) {*\
*from ('src')*\
*into ('copy/target/doc')*\
*}*

I finalized this task by committing and pushing the alterations to my remote repository by using:
- **git add .**
- **git commit -m “(Resolves #7) Adds a new task of type Copy that makes a backup of the source folder”**
- **git push**


Task 4 - Add a new task of type Zip to be used to make an archive.
===================

Lastly and similarly to the above, I created a task type Zip on build.gradle with the following code:
>*task zip (type:Zip) {*\
*from('src')*\
*archiveName 'Zip_test.zip'*\
*destinationDir(file('build'))*\
*}*

I finalized this task by committing and pushing the alterations to my remote repository by using:
- **git add .**
- **git commit -m “(Resolves #8) Creation of task type Zip to make an archive”**
- **git push**

Once the assignment was finalized the Readme.md file in the folder Part1 was updated, committed and pushed to the remote repository using the following commands:
- **git add .**
- **git commit -m "(Resolves #9) Update Readme.md file in folder Part1 of CA2"**
- **git push**

Finally, a tag was created to mark the last commit and the end of the assignment:
- **git tag -a ca2-part1 -m "end of CA2-Part1 assignment"**
- **git push origin ca2-part1**
