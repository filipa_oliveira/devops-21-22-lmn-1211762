Class Assignment 3 - Part 1
===================

The aim of this assignment is to create a VirtualBox virtual machine with Ubuntu. 

I started this assignment by creating a CA3 folder with a CA3Part1 subfolder inside, and by doing an initial commit with the creation of a ReadMe file. This README file would later on be updated, once the assignment is finished.
- **git mkdir CA3Part1**
- **git add .**
- **git commit -m "CA3 Part 1 Initial Commit (Resolves #19)"**
- **git push**

> IMPORTANT NOTE: the steps and commands to set up and create the Virtual machine are all described in the teacher pdf "Devops05". This includes the creation of a host-only network, networking setup,  network utilities, ssh server and ftp server, installation of git and java, cloning, building and running the Spring Tutorial. For this reason, and to avoid duplicating available information, I opted to exclude those steps in this report.  

> As suggested I started by 'manually' creating my Virtual Machine (VM) using VirtualBox, then I changed the VM settings as described in the lecture. Afterwards I started the VM and installed Ubuntu. In the following image we can see the VM running:
> 
[![Screenshot-2022-04-20-at-17-08-51.png](https://i.postimg.cc/FstWZJMN/Screenshot-2022-04-20-at-17-08-51.png)](https://postimg.cc/G9Qx3935)

> Then I cloned my personal repository to the VM and tried to build and execute the spring boot tutorial basic project. The build was successful, and it was possible to access the application from the Host as we can see in the following image:
> 
[![Screenshot-2022-04-20-at-17-06-42.png](https://i.postimg.cc/yYPmZgBf/Screenshot-2022-04-20-at-17-06-42.png)](https://postimg.cc/svMGF2MS)

> Afterwards, I ran the gradle_basic_demo project from the previous assignment, the simple chat application. I executed and ran the "Client" in my host machine terminal "[filipaoliveira@Filipas-Macbook-Pro gradle_basic_demo%: by typing the command:
> 
- **./gradlew runClient**


  [![Screenshot-2022-04-20-at-17-06-05.png](https://i.postimg.cc/bJcDMppD/Screenshot-2022-04-20-at-17-06-05.png)](https://postimg.cc/zL7GHmL8)

> At the same time, I executed and ran the server inside the VM I previously created, by using the VM terminal "filipa1211762@ubuntu:~gradle_basic_demo$" and typing the command:
> 
- **./gradlew runServer**

[![Screenshot-2022-04-20-at-17-08-01.png](https://i.postimg.cc/7YsC4gqy/Screenshot-2022-04-20-at-17-08-01.png)](https://postimg.cc/VdtsB0S7)

>In the chat application, the virtual machine is very useful because it allows me to run an operating system that behaves like a completely separate computer making it possible for me to, together with my host machine terminal, test the application in real-time, I just need to 'log in' and simulate a conversation between two different users as we can see in the following images:

[![Screenshot-2022-04-20-at-17-07-36.png](https://i.postimg.cc/WzVfjXBN/Screenshot-2022-04-20-at-17-07-36.png)](https://postimg.cc/S2ZGf7Q3)

[![Screenshot-2022-04-20-at-17-07-03.png](https://i.postimg.cc/C5vCt7Y6/Screenshot-2022-04-20-at-17-07-03.png)](https://postimg.cc/wRmsRh1h)
