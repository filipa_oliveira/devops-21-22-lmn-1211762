Class Assignment 3 - Part 2
===================

The aim of this assignment is to use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle basic version (developed in CA2, Part2).
I decided to separate the part 2 of the CA3 assignment in two parts: A and B. 
Part A describes the tasks described in the lecture 'devops06.pdf' and part B describes the tasks required in 'devops_ca3-part2.pdf'.

Part A
===================

> As previously mentioned, I began this assignment by performing the tasks described in the lecture slides.
>I started by downloading and installing the vagrant version for macOS and ran the command 'vagrant -v' to check if everything was ok. Then I create a folder named vagrant-project-1 to initialize a vagrant project. The command vagrant init creates a new vagrant configuration file containing all relevant VM configuration, called Vagrantfile; and the envimation/ubungtu-xenial is the name of a Vagrant box, a preconfigured VM image available online:
- **vagrant -v**
- **git mkdir vagrant-project-1**
- **gcd vagrant-project-1**
- **vagrant init envimation/ubuntu-xenial** 

[![vagrantup-com.png](https://i.postimg.cc/BQTX0s82/vagrantup-com.png)](https://postimg.cc/dL0q2bj0)

> Then I started the virtual machine and started an SSH session:
- **vagrant up**
- **vagrant ssh**

[![Welcome-to-Ubuntu-16-04-3-LTS-GNULinux-4-4-0-109-generic-x86-64.png](https://i.postimg.cc/Z50tLxh0/Welcome-to-Ubuntu-16-04-3-LTS-GNULinux-4-4-0-109-generic-x86-64.png)](https://postimg.cc/dhMxsd3K)

> Next, I ran the "vagrant status" command, wich tells me whether my Vagrant machine is running, suspended, not created, etc. In my case, and as expected it informed that the vm was running:
- **vagrant status**

[![Current-machine-states.png](https://i.postimg.cc/DftBNQs8/Current-machine-states.png)](https://postimg.cc/DmPrJXxT)

> Then, as suggested, I forwarded a port from the VM to my machine and setup a static private IP on the VM as per the vagrant file available at: https://github.com/atb/vagrant-basic-example. I also made the VM webserver serve pages from a shared folder (between the host and the guest).
> Because all these modifications were made in the Vagrantfile it was necessary to reload and provision the VM.
> Since I still did not had the html folder on my vagrant project folder I created it and only then I ran the commands:
- **mkdir html**
- **vagrant reload --provision**

[![Clearing-any-previously.png](https://i.postimg.cc/VNY7RYtg/Clearing-any-previously.png)](https://postimg.cc/tsMtRG2n)

To finalize the tasks indicated in the lecture slides, I stopped my VM using the terminal and it powered off the vm in virtualBox:
- **vagrant halt**

[![default-Attempting-graceful-shutdown-of-v-M.png](https://i.postimg.cc/d0L5X5WZ/default-Attempting-graceful-shutdown-of-v-M.png)](https://postimg.cc/2qNdL7NC)
[![vagrant-Project-d.png](https://i.postimg.cc/WpMXQF78/vagrant-Project-d.png)](https://postimg.cc/G9mFDpVT)

> To save space on the host machine, I destroyed the VM and removed all the VM files using the command:
- **vagrant destroy -f**

[![Screenshot-2022-04-27-at-15-49-38.png](https://i.postimg.cc/XvLyfWKw/Screenshot-2022-04-27-at-15-49-38.png)](https://postimg.cc/kVV4Ckp4)

> At the end I removed the box from my local file system, by running the command:
- **vagrant box remove envimation/ubuntu-xenial**

[![0-3-1516241473-with.png](https://i.postimg.cc/gcCHsvL4/0-3-1516241473-with.png)](https://postimg.cc/4n1cNH5h)

IMPORTANT NOTE: it is always possible to create the VM again, just by running the "vagrant up" command.
____________________

Part B
===================

> As suggested, I used the https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution. I cloned it to my computer and copy-pasted it into my CA3Part2 folder:
- **git add.**
- **git commit -m "(Resolves #20) clone folder VagrantFile from folder vagrant-multi-spring-tut-demo into CA3Part2 folder"**
- **git push**

> Then I opened the vagrantFile with VSCode to study the Vagrantfile and see how it was used to create and provision 2 VMs:
> web - this VM is used to run tomcat and the spring boot basic application
> db - this VM is used to execute the H2 server database

> Next, I updated the Vagrantfile configuration so that it issued my own gradle version of the spring application, I started by changing my repository settings from private to public (this step was necessary because when we 'git clone' a private repository, git will ask for authentication using a prompt for reading the password. This prompt for reading the password will break the provision script, since it should execute without any user interaction. The simplest way to avoid this issue is to make the repository public).
> I used the following commands to clone my own repository:
- **git clone https://Filipa_Oliveira@bitbucket.org/filipa_oliveira/devops-21-22-lmn-1211762.git**
- **cd devops-21-22-lmn-1211762/CA2/Part2/react-and-spring-data-rest-basic**
- **chmod u+x gradlew**
- **./gradlew clean build**
> To deploy the war file to tomcat8 I did the following command:
- **sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps**

> Afterwards I initiated my VM at CA3Part2 folder terminal:
- **vagrant up**

[![56-hidden-modules.png](https://i.postimg.cc/wjJ95KzV/56-hidden-modules.png)](https://postimg.cc/hXDFg5fz)

> Since the build was successful, as we can see in the image above, I sent the modifications of the Vagrantfile to the remote repository:
- **git add.**
- **git commit -m "(Resolves #21) Copy & Update Vagrantfile"**
- **git push**

> IMPORTANT NOTE: Since many alterations were being made I decided to copy the react-and-spring-data-rest-basic folder from CA2Part2 into my CA3Part2 folder and committed the changes and changed the path in the Vagrantfile from “devops-21-22-lmn-1211762/CA2/Part2/react-and-spring-data-rest-basic” to “devops-21-22-lmn-1211762/CA3/CA3Part2/react-and-spring-data-rest-basic”
- **git add.**
- **git commit -m "(Resolves #22) Copy react-and-spring-data-rest-basic from CA2Part2 to Folder CA3Part2"**
- **git push**

> Next, I opted to create a new branch named 'update-basic-gradle' and follow the modifications suggested in the teacher's repository, which represent the following commits in my repository:
- **(Resolves #23) ADD: Support for building war file.**
- **(Resolves #24) ADD: Support for h2 console.**
- **(Resolves #25) ADD: weballowothers to h2.**
- **(Resolves #26) ADD: Application context path.**
- **(Resolves #27) UPDATE: Fixes ref to css in index.html**
- **(Resolves #28) ADD: Settings for remote h2 database.**
- **(Resolves #29) UPDATE: Fixes h2 connection string.**
- **(Resolves #30) UPDATE: So that spring will not drop the database on every execution**
- **(Resolves #31) UPDATE properties file to use the new ip address of the db VM**

Afterwards, I ran the vagrant up command on the terminal and opened virtualBox to see if both VMs (web and db) were running:

[![Preview.png](https://i.postimg.cc/7ZkT2rJ3/Preview.png)](https://postimg.cc/dL50bXB3)

In order to make the applications work, I needed to make some more modifications/commits: 
- change the jdk version (from version 11 to 8) in build.gradle 
- update the .gitignore file
- Update Vagrantfile with new cd to deploy the war file
- Add command: rm -R devops-21-22-lmn-1211762 to Vagrantfile
- Change servlet context path to: react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
- Update path in app.js

  [![Screenshot-2022-05-02-at-18-00-40.png](https://i.postimg.cc/5tx3VCxF/Screenshot-2022-05-02-at-18-00-40.png)](https://postimg.cc/yDb9hW3V)

> IMPORTANT NOTE: Since changes were made in the Vagrantfile, it was necessary to stop, initiate and provision the VMs:
- **vagrant halt**
- **vagrant up**
- **vagrant provision**

Finally, I was able to have the applications working:

[![Screenshot-2022-05-02-at-17-12-27.png](https://i.postimg.cc/L8dSNHmW/Screenshot-2022-05-02-at-17-12-27.png)](https://postimg.cc/dL9g0ckR)

[![Screenshot-2022-05-02-at-18-34-41.png](https://i.postimg.cc/tRhNYkYV/Screenshot-2022-05-02-at-18-34-41.png)](https://postimg.cc/mhgHnQ3b)

[![Screenshot-2022-05-02-at-18-37-19.png](https://i.postimg.cc/R0ZvngSC/Screenshot-2022-05-02-at-18-37-19.png)](https://postimg.cc/WdCBB6qx)

> Since everything was working, I merged the 'update-basic-gradle' into the master branch:
- **git checkout master**
- **git merge update-basic-gradle**
- 
____________________

Alternative analysis and implementation: VMware Fusion
===================

> As an alternative, I decided to use VMware, more specifically VMware Fusion for mac. When choosing this alternative I had two main concerns:
- Is it free/open source?
- Does it support a with range of operating systems?

Although VMware Fusion is paid, it does provide a free non-commercial/personal use license, which is the one I used:

[![Screenshot-2022-05-02-at-15-55-59.png](https://i.postimg.cc/QNYKFFc1/Screenshot-2022-05-02-at-15-55-59.png)](https://postimg.cc/1463T5C3)

I started by creating a CA3Part2_Alternativa subfolder inside my CA3 folder in my repository, and copy/pasting the application and the VagrantFile in it.
Next, it was necessary to make some modifications in the Vagrantfile:
- update path to folder ca3part2_alternativa:   cd devops-21-22-lmn-1211762/CA3/CA3Part2_Alternativa/react-and-spring-data-rest-basic
- change the vagrant provider from virtualbox to vmware fusion:   config.vm.provider "vmware_fusion" do |v|


>Afterwards, I started and provisioned the VM, but an error message "An active machine was found with a different provider" was encountered.
In my first attempt to solve the error I decided to modify the VMs names in the Vagrantfile:
- config.vm.define "database" do |database|
- config.vm.define "webserver" do |webserver|

Unfortunately, the error persisted:

[![Screenshot-2022-05-03-at-19-26-01.png](https://i.postimg.cc/4d6CrHZM/Screenshot-2022-05-03-at-19-26-01.png)](https://postimg.cc/ftRgdLVj)

>In my second attempt to solve the error I decided to eliminate all the VMs I had created by using the command:
- **vagrant destroy -f database**

>A new error message was encountered "Vagrant encountered an error while attempting to load the utility:

[![Screenshot-2022-05-03-at-19-29-13.png](https://i.postimg.cc/q77S1DZN/Screenshot-2022-05-03-at-19-29-13.png)](https://postimg.cc/LJrxXDSS)

>To solve this error it was necessary to install Vagrant VMware Utility:

[![Screenshot-2022-05-03-at-19-29-04.png](https://i.postimg.cc/VkqNWv07/Screenshot-2022-05-03-at-19-29-04.png)](https://postimg.cc/LJsRHHTf)

>After the installation, the error persisted, so I decided to shut down and restart my computer. Next, I opened the ca3part2_alternativa folder terminal and run the command:
- **vagrant up --provider=vmware_fusion**

>This time both VMs (database and webserver) were initialized:

[![Screenshot-2022-05-03-at-19-57-11.png](https://i.postimg.cc/tJr2n9pD/Screenshot-2022-05-03-at-19-57-11.png)](https://postimg.cc/0KwpLR6J)

Later on, a new error was encountered "The ssh command responded with a non-zero exit status":

[![Screenshot-2022-05-03-at-20-01-18.png](https://i.postimg.cc/05dQRby7/Screenshot-2022-05-03-at-20-01-18.png)](https://postimg.cc/s1xy5fS2)

After verifying that the path directory were correct in the vagrant file, and that the war files existed in the 'libs' folder in the remote repository, I decided to eliminate the file .vagrant from CA3Part2_Alternativa folder:

[![Last-login-Tue-May-3-195424-on-ttys000.png](https://i.postimg.cc/Z50KdHFL/Last-login-Tue-May-3-195424-on-ttys000.png)](https://postimg.cc/nXbJ8vWs)

> After all the attempts I was able to successfully put both VMs running:

[![Screenshot-2022-05-08-at-16-54-46.png](https://i.postimg.cc/661rZjCH/Screenshot-2022-05-08-at-16-54-46.png)](https://postimg.cc/4nVHk1Lt)



Alternative analysis and comparison: VirtualBox vs VMware Fusion
===================

This is a simple overview of a side to side comparison of some important features of both providers:

| Comparison      | VirtualBox |  VMware 
| ----------- | ----------- | ----------- |
|  Software Virtualization   | Yes       | No       |
| Hardware Virtualization   | Yes       | No       |
| Host Operating Systems	 |  Linux, Windows, Solaris, macOS, FreeBSD | Linux, Windows + macOS (requires VMware Fusion) |
| Guest Operating Systems | Linux, Windows, Solaris, macOS, FreeBSD | Linux, Windows, Solaris, FreeBSD + macOS (with VMware Fusion) |
|  Snapshots |  Yes | Snapshots only supported on paid virtualization products, not on VMware Workstation Player |
| Cost and Licenses | 	Free, under the GNU General Public License | VMware Workstation Player is free, while other VMware products require a paid license |


>There are a few reasons one should use VirtualBox over Fusion. One reason, is if we need to virtualize a workload for commercial purposes and don’t want to pay. Another, is if we need to create clones and don’t want to pay for Fusion Pro.
>Although, after using and testing both providers, I found that it was more difficult for me to use the VMware fusion probably because I did not have the teacher documentation and guidance and had to figure out and solve all the errors encountered alone, but I do think that VMware fusion has some great advantages: it has more features and supports more guest operating systems, and it has a user interface that I, as anApple users am familiar with.


